import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { CommitComponent } from './components/commit/commit.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'users/:user', component: UserComponent },
  { path: 'repos/:user/:repo', component: RepositoryComponent, pathMatch: 'full' },
  { path: 'repos/:user/:repo/:sha', component: CommitComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes), RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}