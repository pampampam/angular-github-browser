export class User {
  login: string;
  avatar_url: string;
  public_repos: number;
}