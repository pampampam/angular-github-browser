export class Repository {
  name: string;
  description: string;
  default_branch: string;
}