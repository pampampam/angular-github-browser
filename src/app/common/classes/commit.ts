export class Commit {
  sha: string;
  parents: {
    sha: string;
  }[];
  commit: {
    message: string;
    commiter: {
      name: string;
      date: string;
    }
  }
}