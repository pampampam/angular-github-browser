import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { server, auth } from '../common/const';
import { User } from '../common/classes/user';

@Injectable()
export class UserService {
  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error(error);
    return Promise.reject(error.message || error);
  }

  get(user: string): Promise<User> {
    return this.http.get(
      `${server}users/${user}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }
}