import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { server, auth } from '../common/const';

@Injectable()
export class SearchService {
  constructor(private http: Http) {}

  private handleError(error: any): Promise<any> {
    console.error(error);
    return Promise.reject(error.message || error);
  }

  search(searchText: string, entity: string, page: string): Promise<Object[]> {
    if (!page) {
      page = '1';
    }
    
    return this.http.get(
      `${server}search/${entity}?q=${searchText}&page=${page}&per_page=10`,
      { headers: auth }
    )
      .toPromise()
      .then(response => {
        const parsed = response.json();

        return {
          items: parsed.items,
          count: parsed.total_count
        };
      })
      .catch(this.handleError);
  }
}