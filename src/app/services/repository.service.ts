import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { server, auth } from '../common/const';
import { Repository } from '../common/classes/repository';
import { Branch } from '../common/classes/branch';
import { Commit } from '../common/classes/commit';

@Injectable()
export class RepositoryService {
  constructor(private http: Http) { }
  
  private handleError(error: any): Promise<any> {
    console.error(error);
    return Promise.reject(error.message || error);
  }

  private async listCommits(user: string, repo: string, sha: string, counter: number): Promise<Commit[]> {
    if (counter == 0)
      return [];

    let commit: Commit;
    let parents: Commit[] = [];

    await this.http.get(
      `${server}repos/${user}/${repo}/commits/${sha}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => commit = response.json() as Commit)
      .catch(this.handleError);


    for (let i = 0; i < commit.parents.length; ++i) {
      await this.listCommits(user, repo, commit.parents[i].sha, counter - 1)
        .then(response => parents = parents.concat(response));
    }

    return [commit, ...parents];
  }

  list(user: string, page: string): Promise<Repository[]> {
    return this.http.get(
      `${server}users/${user}/repos?per_page=10&page=${page}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => response.json() as Repository[])
      .catch(this.handleError);
  }

  get(user: string, repo: string): Promise<Repository> {
    return this.http.get(
      `${server}repos/${user}/${repo}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => response.json() as Repository)
      .catch(this.handleError);
  }

  listBranches(user: string, repo: string): Promise<Branch[]> {
    return this.http.get(
      `${server}repos/${user}/${repo}/branches`,
      { headers: auth }
    )
      .toPromise()
      .then(response => response.json() as Branch[])
      .catch(this.handleError);
  }

  getBranch(user: string, repo: string, branch: string): Promise<Object> {
    return this.http.get(
      `${server}repos/${user}/${repo}/branches/${branch}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => {
        const branch = response.json();
        return this.listCommits(user, repo, branch.commit.sha, 10)
          .then(response => {
            return {
              commits: response as Commit[],
              hasMore: response.length && response[response.length - 1].parents.length
            };
          });
      })
      .catch(this.handleError)
  }

  listOlderCommits(user: string, repo: string, sha: string): Promise<Object> {
    return this.listCommits(user, repo, sha, 10)
      .then(response => {
        return {
          commits: response,
          hasMore: response.length && response[response.length - 1].parents.length
        };
      });
  }

  getCommit(user: string, repo: string, sha: string): Promise<Commit> {
    return this.http.get(
      `${server}repos/${user}/${repo}/commits/${sha}`,
      { headers: auth }
    )
      .toPromise()
      .then(response => response.json() as Commit)
      .catch(this.handleError);
  }
}