import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RepositoryComponent } from './components/repository/repository.component';
import { CommitComponent } from './components/commit/commit.component';

const repoRoutes: Routes = [
  { path: 'repos/:user/:repo', component: RepositoryComponent,pathMatch: 'full', children: [
    { path: ':sha', component: CommitComponent }
  ]}
];

@NgModule({
  imports: [ RouterModule.forChild(repoRoutes) ],
  exports: [ RouterModule ]
})
export class RepoRoutingModule {}