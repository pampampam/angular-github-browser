import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { RepoRoutingModule } from './repo-routing.module';
import { MaterialModule } from './material.module';

import { AppComponent } from './components/app/app.component';
import { HomeComponent} from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { CommitComponent } from './components/commit/commit.component';
import { SearchService } from './services/search.service';
import { UserService } from './services/user.service';
import { RepositoryService } from './services/repository.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    MaterialModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    RepositoryComponent,
    CommitComponent
  ],
  providers: [
    SearchService,
    UserService,
    RepositoryService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}