import { Component } from '@angular/core';
import { Router, ActivatedRoute, GuardsCheckEnd  } from '@angular/router';

@Component({
  selector: 'app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'Github Browser';
  navList: { name: string, url: string }[];

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
    router.events.subscribe((event: GuardsCheckEnd) => {
      if (event.shouldActivate) {
        const routes = event.url.split('/');
        routes.shift();
        const entity = routes.shift();

        if (entity === 'users') {
          const user = routes.shift().split('?').shift();
          this.navList = [
            { name: user, url: `users/${user}`}
          ];
        } else if (entity === 'repos') {
          const user = routes.shift();
          const repo = routes.shift().split('?').shift();
          this.navList = [
            { name: user, url: `users/${user}` },
            { name: repo, url: `repos/${user}/${repo}` }
          ];
        } else {
          this.navList = [];
        }
      }
    })
  }
}