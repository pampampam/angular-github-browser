import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { RepositoryService } from '../../services/repository.service';

@Component({
  selector: 'commit',
  templateUrl: './commit.component.html'
})

export class CommitComponent implements OnInit {
  commit: Object;

  constructor(
    private repositoryService: RepositoryService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const params = this.route.snapshot.paramMap;

    this.repositoryService.getCommit(params.get('user'), params.get('repo'), params.get('sha'))
      .then((response: any) => this.commit = response);
  }

  parseFile(fileText: string): string[] {
    return fileText ? fileText.split('\n') : ['No changes'];
  }
}