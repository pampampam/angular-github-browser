import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { RepositoryService } from '../../services/repository.service';
import { Repository } from '../../common/classes/repository';
import { Branch } from '../../common/classes/branch';
import { Commit } from '../../common/classes/commit';

@Component({
  selector: 'repository',
  templateUrl: './repository.component.html'
})

export class RepositoryComponent implements OnInit {
  repo: Repository;
  selectedBranch: string;
  branches: Branch[];
  commits: Commit[];
  hasMore: boolean;
  isFetching: boolean;

  constructor(
    private repositoryService: RepositoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const params = this.route.snapshot.paramMap;

    this.repositoryService.get(params.get('user'), params.get('repo'))
      .then((repo: Repository) => {
        this.repo = repo;
        this.selectedBranch = repo.default_branch;
        this.isFetching = true;
        
        this.repositoryService.getBranch(params.get('user'), params.get('repo'), repo.default_branch)
          .then((branch: any) => {
            this.hasMore = branch.hasMore;
            this.commits = branch.commits;
            this.isFetching = false;
          });
      });

    this.repositoryService.listBranches(params.get('user'), params.get('repo'))
      .then((response: Branch[]) => this.branches = response);
  }

  onBranchSelect(branch: string): void {
    const params = this.route.snapshot.paramMap;

    this.commits = [];
    this.repositoryService.getBranch(params.get('user'), params.get('repo'), branch)
      .then((response: any) => {
        this.hasMore = response.hasMore;
        this.commits = response.commits;
      });
  }

  onCommitClick(commit: Commit): void {
    this.router.navigate([commit.sha], {relativeTo: this.route});
  }

  loadMore(): void {
    const params = this.route.snapshot.paramMap;

    this.isFetching = true;
    this.repositoryService.listOlderCommits(
      params.get('user'),
      params.get('repo'),
      this.commits[this.commits.length - 1].parents[0].sha
    )
      .then((response: any) => {
        this.hasMore = response.hasMore;
        this.commits = this.commits.concat(response.commits);
        this.isFetching = false;
      });
  }
}