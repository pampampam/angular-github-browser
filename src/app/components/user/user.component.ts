import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { ActivatedRoute, Router, Params, ParamMap } from '@angular/router';

import { UserService } from '../../services/user.service';
import { RepositoryService } from '../../services/repository.service';
import { User } from '../../common/classes/user';
import { Repository } from '../../common/classes/repository';

@Component({
  selector: 'user',
  templateUrl: './user.component.html'
})

export class UserComponent implements OnInit {
  user: User;
  repos: Repository[];
  page: number;
  isFetching: boolean;

  constructor(
    private userService: UserService,
    private repositoryService: RepositoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((queryParams: ParamMap) =>{
      const params = this.route.snapshot.paramMap;
      const page = +queryParams.get('page');
  
      this.page = page || 1;
      this.getUser(params.get('user'));
      this.listRepos(params.get('user'), page || null);
    });
  }

  getUser(user: string): void {
    this.userService.get(user)
      .then((user: User) => this.user = user);
  }

  listRepos(user: string, page: number): void {
    const queryParams: Params = {
      page: page && page.toString()
    };

    this.isFetching = true;
    this.router.navigate(['/users', user], { queryParams: queryParams });
    this.repositoryService.list(user, queryParams.page)
      .then((response: Repository[]) => {
        this.repos = response;
        this.isFetching = false;
      });
  }

  onRepoClick(repo: Repository): void {
    this.router.navigate(['/repos', this.user.login, repo.name]);
  }

  leaf(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.listRepos(this.user.login, this.page);
  }
}