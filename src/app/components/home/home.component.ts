import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { ActivatedRoute, Router, Params, ParamMap } from '@angular/router';

import { SearchService } from '../../services/search.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit{
  title: string;
  radio: string;
  entity: string;
  searchText: string;
  items: Object[];
  isFetching: boolean;
  itemsTotalCount: number;
  notFound: boolean;
  page: number;

  constructor(
    private searchService: SearchService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.title = 'Home';
    this.radio = 'users';
    this.notFound = false;
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((params: ParamMap) => {
      if (params.has('entity') && params.has('text')) {
        const searchText: string = params.get('text');
        const entity: string = params.get('entity');
        const page: string = params.get('page');
  
        this.searchText = searchText;
        this.entity = entity;
        this.radio = entity;
        this.page = +page;
        this.search(searchText, entity, page);
      }
    });
  }

  onItemClick(item: any): void {
    if (this.entity === 'users') {
      this.router.navigate(['/users', item.login]);
    } else if (this.entity === 'repositories') {
      this.router.navigate(['/repos', item.owner.login, item.name]);
    }
  }

  search(searchText: string, entity: string, page: string): void {
    if (searchText === '')
      return;

    const queryParams: Params = {
      entity: entity,
      text: searchText,
      page: page
    };

    this.entity = entity;
    if (!page) {
      this.page = 1;
    }

    this.isFetching = true;
    this.items = [];
    this.router.navigate(['/home'], { queryParams: queryParams });

    this.searchService.search(searchText, entity, page)
      .then((response: any) => {
        this.items = response.items;
        this.itemsTotalCount = response.count;
        this.notFound = response.items.length === 0;
        this.isFetching = false;
      });
  }

  leaf(event: PageEvent, searchText: string, entity: string) {
    this.page = event.pageIndex + 1;
    this.search(searchText, entity, this.page.toString())
  }
}